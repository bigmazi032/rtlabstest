package ru.zinyakov.rtlabstest.security;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class CustomUserDetailsService implements UserDetailsService {


    @Override
    public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {
        User user = new User().setName("user").setPassword("$2a$10$QWpbVYUzltN4oSOZEMjsV.N3QO2XK6EC4TB8AKfKiYwhTg2eQlnnm");
        if (user == null) {
            throw new UsernameNotFoundException("User " + name + " not found");
        } else {
            return new CustomUserDetails(user);
        }
    }


}


@Data
@Accessors(chain = true)
@RequiredArgsConstructor
class User {
    private String name;
    private String password;
}