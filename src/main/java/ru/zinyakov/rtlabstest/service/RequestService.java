package ru.zinyakov.rtlabstest.service;

import ru.zinyakov.rtlabstest.service.dto.RequestDto;
import ru.zinyakov.rtlabstest.service.dto.FormSearchRequestDto;

import java.util.List;

public interface RequestService {
    Long add(RequestDto requestDto);

    List<RequestDto> getAll();

    RequestDto findById(Long id);

    void update(RequestDto requestDto);

    void updateStatus(Long id, Boolean status);

    List<RequestDto> findAllBySpecification(FormSearchRequestDto searchCriteriaForRequest);

}
