package ru.zinyakov.rtlabstest.service;

import ru.zinyakov.rtlabstest.service.dto.ApplicantDto;

import java.util.List;

public interface ApplicantService {
    ApplicantDto add(ApplicantDto applicantDto);

    List<ApplicantDto> getAll();

    ApplicantDto findById(Long id);

    ApplicantDto findByPasportNumber(String pasportNumber);
}


