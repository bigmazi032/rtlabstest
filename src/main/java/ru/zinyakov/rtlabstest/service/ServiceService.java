package ru.zinyakov.rtlabstest.service;

import ru.zinyakov.rtlabstest.service.dto.ServiceDto;

import java.util.List;

public interface ServiceService {
    Long add(ServiceDto serviceDto);

    List<ServiceDto> getAll();

    ServiceDto findById(Long id);

}
