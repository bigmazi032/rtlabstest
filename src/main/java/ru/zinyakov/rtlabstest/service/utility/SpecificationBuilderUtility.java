package ru.zinyakov.rtlabstest.service.utility;

import ru.zinyakov.rtlabstest.db.model.Request;
import ru.zinyakov.rtlabstest.db.repository.specification.SpecificationBuilder;
import ru.zinyakov.rtlabstest.service.dto.FormSearchRequestDto;

public class SpecificationBuilderUtility {

    public static SpecificationBuilder<Request> buildRequestSpecification(FormSearchRequestDto formSearchRequestDto) {

        SpecificationBuilder<Request> builder = new SpecificationBuilder<>();

        if (formSearchRequestDto.getCreateDateFrom() != null) {
            builder.add("createDateFrom", formSearchRequestDto.getCreateDateFrom());
        }
        if (formSearchRequestDto.getCreateDateTo() != null) {
            builder.add("createDateTo", formSearchRequestDto.getCreateDateTo());
        }
        if (formSearchRequestDto.getStatus() != null) {
            builder.add("status", formSearchRequestDto.getStatus());
        }
        if (formSearchRequestDto.getServiceType() != null) {
            builder.add("serviceType", formSearchRequestDto.getServiceType());
        }
        if (formSearchRequestDto.getApplicantFirstName() != null) {
            builder.add("applicantFirstName", formSearchRequestDto.getApplicantFirstName());
        }
        if (formSearchRequestDto.getApplicantLastName() != null) {
            builder.add("applicantLastName", formSearchRequestDto.getApplicantLastName());
        }
        if (formSearchRequestDto.getApplicantMiddleName() != null) {
            builder.add("applicantMiddleName", formSearchRequestDto.getApplicantMiddleName());
        }
        if (formSearchRequestDto.getApplicantPasportNumber() != null) {
            builder.add("applicantPasportNumber", formSearchRequestDto.getApplicantPasportNumber());
        }

        return builder;
    }


}

