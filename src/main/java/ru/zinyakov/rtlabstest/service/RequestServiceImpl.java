package ru.zinyakov.rtlabstest.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.zinyakov.rtlabstest.db.model.Request;
import ru.zinyakov.rtlabstest.db.repository.RequestRepository;
import ru.zinyakov.rtlabstest.db.repository.specification.SpecificationBuilder;
import ru.zinyakov.rtlabstest.service.converter.Dto2Request;
import ru.zinyakov.rtlabstest.service.converter.Model2RequestDto;
import ru.zinyakov.rtlabstest.service.dto.FormSearchRequestDto;
import ru.zinyakov.rtlabstest.service.dto.RequestDto;
import ru.zinyakov.rtlabstest.web.exception.ThereIsNoSuchResourceException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static ru.zinyakov.rtlabstest.service.utility.SpecificationBuilderUtility.buildRequestSpecification;

@Service
@AllArgsConstructor
public class RequestServiceImpl implements RequestService {

    private final RequestRepository requestRepository;


    @Override
    public Long add(RequestDto requestDto) {
        return requestRepository.save(Dto2Request.convert(requestDto)).getId();
    }

    @Override
    public List<RequestDto> getAll() {

        return requestRepository.findAll().stream().map(Model2RequestDto::convert).collect(Collectors.toList());
    }

    @Override
    public RequestDto findById(Long id) {
        Optional<Request> optionalRequest = requestRepository.findById(id);
        if (!optionalRequest.isPresent()) {
            throw new ThereIsNoSuchResourceException();
        }

        return Model2RequestDto.convert(optionalRequest.get());

    }

    @Override
    public void update(RequestDto requestDto) {
        requestRepository.save(Dto2Request.convert(requestDto));

    }

    @Override
    public void updateStatus(Long id, Boolean status) {
        Optional<Request> optionalRequest = requestRepository.findById(id);
        if (!optionalRequest.isPresent()) {
            throw new ThereIsNoSuchResourceException();
        }
        requestRepository.save(optionalRequest.get().setStatus(status));

    }

    @Override
    public List<RequestDto> findAllBySpecification(FormSearchRequestDto formSearchRequestDto) {

        SpecificationBuilder<Request> builder = buildRequestSpecification(formSearchRequestDto);

        return requestRepository.findAll(builder.buildSpecification()).stream().map(Model2RequestDto::convert).collect(Collectors.toList());
    }


}
