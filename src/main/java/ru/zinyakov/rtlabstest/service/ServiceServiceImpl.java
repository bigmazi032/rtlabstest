package ru.zinyakov.rtlabstest.service;

import lombok.AllArgsConstructor;
import ru.zinyakov.rtlabstest.db.model.Service;
import ru.zinyakov.rtlabstest.db.repository.ServiceRepository;
import ru.zinyakov.rtlabstest.service.converter.Model2ServiceDto;
import ru.zinyakov.rtlabstest.service.dto.ServiceDto;
import ru.zinyakov.rtlabstest.web.exception.ThereIsNoSuchResourceException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@org.springframework.stereotype.Service
@AllArgsConstructor
public class ServiceServiceImpl implements ServiceService {

    private final ServiceRepository serviceRepository;


    @Override
    public Long add(ServiceDto serviceDto) {

        Service serviceDb = new Service()
                .setId(serviceDto.getId())
                .setType(serviceDto.getType());

        return serviceRepository.save(serviceDb).getId();
    }

    @Override
    public ServiceDto findById(Long id) {
        Optional<Service> optionalService = serviceRepository.findById(id);
        if (!optionalService.isPresent()) {
            throw new ThereIsNoSuchResourceException();
        }
        return Model2ServiceDto.convert(optionalService.get());
    }

    @Override
    public List<ServiceDto> getAll() {

        return serviceRepository.findAll().stream().map(Model2ServiceDto::convert).collect(Collectors.toList());
    }


}
