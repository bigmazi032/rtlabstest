package ru.zinyakov.rtlabstest.service.converter;

import ru.zinyakov.rtlabstest.db.model.Applicant;
import ru.zinyakov.rtlabstest.service.dto.ApplicantDto;
import ru.zinyakov.rtlabstest.web.exception.IncorrectInputException;

public class Dto2Applicant {
    public static Applicant convert(ApplicantDto applicantDto){
        if (applicantDto==null) {
            throw  new IncorrectInputException();
        }
        return new Applicant()
                .setId(applicantDto.getId())
                .setFirstName(applicantDto.getFirstName())
                .setLastName(applicantDto.getLastName())
                .setMiddleName(applicantDto.getMiddleName())
                .setPasportNumber(applicantDto.getPasportNumber());
    }

}
