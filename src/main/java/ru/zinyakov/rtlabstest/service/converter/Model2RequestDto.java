package ru.zinyakov.rtlabstest.service.converter;

import ru.zinyakov.rtlabstest.db.model.Request;
import ru.zinyakov.rtlabstest.service.dto.RequestDto;

import java.util.stream.Collectors;

public class Model2RequestDto{
    public static RequestDto convert (Request request){
        return new RequestDto()
                .setId(request.getId())
                .setCreateDate(request.getCreateDate())
                .setService(Model2ServiceDto.convert(request.getService()))
                .setApplicant(Model2ApplicantDto.convert(request.getApplicant()))
                .setStatus(request.getStatus())
                .setAttachedFiles(request.getAttachedFiles().stream().map(Model2AttachedFileDto::convert).collect(Collectors.toList()));

    }

}
