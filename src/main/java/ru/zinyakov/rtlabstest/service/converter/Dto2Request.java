package ru.zinyakov.rtlabstest.service.converter;

import ru.zinyakov.rtlabstest.db.model.Request;
import ru.zinyakov.rtlabstest.service.dto.RequestDto;
import ru.zinyakov.rtlabstest.web.exception.IncorrectInputException;

public  class Dto2Request {
    public static Request convert (RequestDto requestDto) {
        if (requestDto==null) {
            throw  new IncorrectInputException();
        }

     return new Request()
                .setId(requestDto.getId())
                .setCreateDate(requestDto.getCreateDate())
                .setApplicant(Dto2Applicant.convert(requestDto.getApplicant()))
                .setService(Dto2Service.convert(requestDto.getService()))
                .setStatus(requestDto.getStatus());
    }

}
