package ru.zinyakov.rtlabstest.service.converter;

import ru.zinyakov.rtlabstest.db.model.Service;
import ru.zinyakov.rtlabstest.service.dto.ServiceDto;

public class Model2ServiceDto{
    public static ServiceDto convert(Service service){
        return new ServiceDto()
                .setId(service.getId())
                .setType(service.getType());
    }

}
