package ru.zinyakov.rtlabstest.service.converter;

import ru.zinyakov.rtlabstest.db.model.AttachedFile;
import ru.zinyakov.rtlabstest.service.dto.AttachedFileDto;

public class Model2AttachedFileDto {
    public static AttachedFileDto convert(AttachedFile attachedFile){
        return new AttachedFileDto()
                .setId(attachedFile.getId())
                .setName(attachedFile.getName())
                .setPath(attachedFile.getPath());
    }

}
