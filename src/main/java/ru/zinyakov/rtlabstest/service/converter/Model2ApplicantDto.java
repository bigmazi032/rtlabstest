package ru.zinyakov.rtlabstest.service.converter;

import ru.zinyakov.rtlabstest.db.model.Applicant;
import ru.zinyakov.rtlabstest.service.dto.ApplicantDto;

public class Model2ApplicantDto {
    public static ApplicantDto convert(Applicant applicant){
        return new ApplicantDto()
                .setId(applicant.getId())
                .setFirstName(applicant.getFirstName())
                .setLastName(applicant.getLastName())
                .setMiddleName(applicant.getMiddleName())
                .setPasportNumber(applicant.getPasportNumber());
    }

}
