package ru.zinyakov.rtlabstest.service.converter;

import ru.zinyakov.rtlabstest.db.model.Service;
import ru.zinyakov.rtlabstest.service.dto.ServiceDto;
import ru.zinyakov.rtlabstest.web.exception.IncorrectInputException;

public class Dto2Service {
    public static Service convert(ServiceDto serviceDto){
        if (serviceDto==null) {
            throw  new IncorrectInputException();
        }
        return new Service()
                .setId(serviceDto.getId())
                .setType(serviceDto.getType());
    }

}
