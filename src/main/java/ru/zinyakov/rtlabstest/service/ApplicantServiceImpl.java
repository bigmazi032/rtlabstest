package ru.zinyakov.rtlabstest.service;


import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.zinyakov.rtlabstest.db.model.Applicant;
import ru.zinyakov.rtlabstest.db.repository.ApplicantRepository;
import ru.zinyakov.rtlabstest.service.converter.Dto2Applicant;
import ru.zinyakov.rtlabstest.service.converter.Model2ApplicantDto;
import ru.zinyakov.rtlabstest.service.dto.ApplicantDto;
import ru.zinyakov.rtlabstest.web.exception.IncorrectInputException;
import ru.zinyakov.rtlabstest.web.exception.ThereIsNoSuchResourceException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ApplicantServiceImpl implements ApplicantService {

    private final ApplicantRepository applicantRepository;

    @Override
    public ApplicantDto add(ApplicantDto applicantDto) {
        if (applicantRepository.findByPasportNumber(applicantDto.getPasportNumber()) != null) {
            throw new IncorrectInputException();
        }
        return applicantDto.setId(applicantRepository.save(Dto2Applicant.convert(applicantDto)).getId());
    }

    @Override
    public List<ApplicantDto> getAll() {
        return applicantRepository.findAll().stream().map(Model2ApplicantDto::convert).collect(Collectors.toList());
    }

    @Override
    public ApplicantDto findById(Long id) {
        Optional<Applicant> optional = applicantRepository.findById(id);
        if (!optional.isPresent()) {
            throw new ThereIsNoSuchResourceException();
        }
        return Model2ApplicantDto.convert(optional.get());
    }

    @Override
    public ApplicantDto findByPasportNumber(String pasportNumber) {
        Applicant applicant = applicantRepository.findByPasportNumber(pasportNumber);
        if (applicant == null) {
            throw new ThereIsNoSuchResourceException();
        }
        return Model2ApplicantDto.convert(applicant);
    }
}
