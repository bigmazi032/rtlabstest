package ru.zinyakov.rtlabstest.service.dto;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class ServiceDto {
    private Long id;
    private String type;
}
