package ru.zinyakov.rtlabstest.service.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDate;

@Data
@Accessors(chain = true)
public class FormSearchRequestDto {
    private LocalDate createDateFrom;
    private LocalDate createDateTo;
    private Boolean status;
    private String serviceType;
    private String applicantFirstName;
    private String applicantLastName;
    private String applicantMiddleName;
    private String applicantPasportNumber;

}
