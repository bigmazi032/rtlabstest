package ru.zinyakov.rtlabstest.service.dto;


import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class ApplicantDto {
    private Long id;
    private String firstName;
    private String lastName;
    private String middleName;
    private String pasportNumber;

}
