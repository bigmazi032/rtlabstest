package ru.zinyakov.rtlabstest.service.dto;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class AttachedFileDto {
    private Long id;
    private String name;
    private String path;

}
