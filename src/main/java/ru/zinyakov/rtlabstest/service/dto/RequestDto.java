package ru.zinyakov.rtlabstest.service.dto;


import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDate;
import java.util.List;

@Data
@Accessors(chain = true)
public class RequestDto {
    private Long id;
    private LocalDate createDate;
    private ServiceDto service;
    private ApplicantDto applicant;
    private Boolean status;
    private List<AttachedFileDto> attachedFiles;

}
