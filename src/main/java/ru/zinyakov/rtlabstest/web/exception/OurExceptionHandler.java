package ru.zinyakov.rtlabstest.web.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class OurExceptionHandler extends ResponseEntityExceptionHandler {


    @ExceptionHandler(ThereIsNoSuchResourceException.class)
    protected ResponseEntity<OurException> handleThereIsNoSuchResourceException() {
        return new ResponseEntity<>(new OurException("There is no such resource"), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(IncorrectInputException.class)
    protected ResponseEntity<OurException> handleIncorrectInputException() {
        return new ResponseEntity<>(new OurException("Incorrect input"), HttpStatus.NOT_FOUND);
    }


    @Data
    @AllArgsConstructor
    private static class OurException {
        private String message;
    }
}
