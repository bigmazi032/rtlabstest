package ru.zinyakov.rtlabstest.web.controller;


import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import ru.zinyakov.rtlabstest.service.ApplicantService;
import ru.zinyakov.rtlabstest.service.dto.ApplicantDto;
import ru.zinyakov.rtlabstest.web.exception.IncorrectInputException;

import java.util.List;


@RestController
@RequestMapping("/applicants")
@RequiredArgsConstructor
class ApplicantController {

    private final ApplicantService applicantService;


    @GetMapping(value = "/{id}")
    public ApplicantDto findById(@PathVariable("id") Long id) {

        return applicantService.findById(id);
    }


    @GetMapping(value = "/byPasport/{pasportNumber}")
    public ApplicantDto findByPasport(@PathVariable("pasportNumber") String pasportNumber) {

        return applicantService.findByPasportNumber(pasportNumber);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ApplicantDto create(@RequestBody ApplicantDto applicantDto) {
        if (applicantDto == null) {
            throw new IncorrectInputException();
        }

        return applicantService.add(applicantDto);
    }

    @Secured({"ROLE_USER"})
    @GetMapping
    public List<ApplicantDto> findAll() {
        return applicantService.getAll();
    }

}


