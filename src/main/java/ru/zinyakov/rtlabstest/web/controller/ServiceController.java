package ru.zinyakov.rtlabstest.web.controller;


import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import ru.zinyakov.rtlabstest.service.ServiceService;
import ru.zinyakov.rtlabstest.service.dto.ServiceDto;
import ru.zinyakov.rtlabstest.web.exception.IncorrectInputException;

import java.util.List;


@RestController
@RequestMapping("/services")
@AllArgsConstructor
class ServiceController {

    private final ServiceService serviceService;


    @GetMapping(value = "/{id}")
    public ServiceDto findById(@PathVariable("id") Long id) {
        return serviceService.findById(id);
    }

    @Secured({"ROLE_USER"})
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Long create(@RequestBody ServiceDto serviceDto) {

        if (serviceDto == null) {
            throw new IncorrectInputException();
        }

        return serviceService.add(serviceDto);
    }

    @GetMapping
    public List<ServiceDto> findAll() {
        return serviceService.getAll();
    }

}


