package ru.zinyakov.rtlabstest.web.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import ru.zinyakov.rtlabstest.service.RequestService;
import ru.zinyakov.rtlabstest.service.dto.FormSearchRequestDto;
import ru.zinyakov.rtlabstest.service.dto.RequestDto;
import ru.zinyakov.rtlabstest.web.exception.IncorrectInputException;

import java.util.List;


@RestController
@RequestMapping("/requests")
@RequiredArgsConstructor
class RequestController {


    private final RequestService requestService;


    @Secured({"ROLE_USER"})
    @GetMapping
    public List<RequestDto> findAll() {
        return requestService.getAll();
    }


    @GetMapping(value = "/{id}")
    public RequestDto findById(@PathVariable("id") Long id) {

        return requestService.findById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Long create(@RequestBody RequestDto requestDto) {
        if (requestDto == null) {
            throw new IncorrectInputException();
        }

        return requestService.add(requestDto);
    }

    @Secured({"ROLE_USER"})
    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    public void update(@RequestBody RequestDto requestDto) {
        if (requestDto == null) {
            throw new IncorrectInputException();
        }

        requestService.update(requestDto);
    }

    @Secured({"ROLE_USER"})
    @PutMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void updateStatus(@PathVariable("id") Long id, @RequestBody RequestDto requestDto) {
        if (requestDto == null) {
            throw new IncorrectInputException();
        }

        requestService.updateStatus(id, requestDto.getStatus());
    }

    @Secured({"ROLE_USER"})
    @RequestMapping(method = RequestMethod.POST, value = "/for")
    @ResponseBody
    public List<RequestDto> findAllBySpecification(@RequestBody FormSearchRequestDto searchCriteriaForRequest) {
        if (searchCriteriaForRequest == null) {
            throw new IncorrectInputException();
        }

        return requestService.findAllBySpecification(searchCriteriaForRequest);
    }

}
