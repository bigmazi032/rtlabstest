package ru.zinyakov.rtlabstest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import ru.zinyakov.rtlabstest.security.CustomUserDetailsService;
import ru.zinyakov.rtlabstest.security.MySavedRequestAwareAuthenticationSuccessHandler;
import ru.zinyakov.rtlabstest.security.RestAuthenticationEntryPoint;

@SpringBootApplication
public class RtlabstestApplication {

    public static void main(String[] args) {
        SpringApplication.run(RtlabstestApplication.class, args);
    }

    @Configuration
    @EnableWebSecurity
    @EnableGlobalMethodSecurity(securedEnabled = true, proxyTargetClass = true)
    @ComponentScan("ru.zinyakov.rtlabstest.security")
    public class SecurityJavaConfig extends WebSecurityConfigurerAdapter {


        @Autowired
        private RestAuthenticationEntryPoint restAuthenticationEntryPoint;

        @Autowired
        private MySavedRequestAwareAuthenticationSuccessHandler mySuccessHandler;

        private SimpleUrlAuthenticationFailureHandler myFailureHandler = new SimpleUrlAuthenticationFailureHandler();


        @Autowired
        CustomUserDetailsService userDetailsService;

        @Autowired
        public void registerGlobalAuthentication(AuthenticationManagerBuilder auth) throws Exception {
            auth.authenticationProvider(authProvider());
        }

        @Bean
        public AuthenticationProvider authProvider() {
            DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
            authProvider.setUserDetailsService(userDetailsService);
            authProvider.setPasswordEncoder(encoder());

            return authProvider;
        }

// НЕ смог выполнить авторизацию с базовыми настройками
//        @Override
//        public void configure(AuthenticationManagerBuilder auth) throws Exception {
//            auth.inMemoryAuthentication()
//                    .withUser("admin").password(encoder().encode("adminPass")).roles("ADMIN")
//                    .and()
//                    .withUser("user").password("userPass").roles("USER");
//        }


        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http
                    .csrf().disable()
                    .exceptionHandling()
                    .authenticationEntryPoint(restAuthenticationEntryPoint)
                    .and()
                    .authorizeRequests()
                    .antMatchers("/api/admin/**").hasRole("ADMIN")
                    .and()
                    .formLogin()
                    .successHandler(mySuccessHandler)
                    .failureHandler(myFailureHandler)
                    .and()
                    .logout();
        }


        @Bean
        public PasswordEncoder encoder() {
            return new BCryptPasswordEncoder();
        }
    }

}
