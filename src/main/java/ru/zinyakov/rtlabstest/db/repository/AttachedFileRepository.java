package ru.zinyakov.rtlabstest.db.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.zinyakov.rtlabstest.db.model.AttachedFile;

public interface AttachedFileRepository extends JpaRepository<AttachedFile, Long> {
}
