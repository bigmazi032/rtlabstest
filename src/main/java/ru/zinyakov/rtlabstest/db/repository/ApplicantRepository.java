package ru.zinyakov.rtlabstest.db.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import ru.zinyakov.rtlabstest.db.model.Applicant;


public interface ApplicantRepository extends JpaRepository<Applicant, Long>, JpaSpecificationExecutor {
    Applicant findByPasportNumber(String pasportNumber);


}
