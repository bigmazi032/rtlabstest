package ru.zinyakov.rtlabstest.db.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.zinyakov.rtlabstest.db.model.Service;

public interface ServiceRepository extends JpaRepository <Service,Long > {

}