package ru.zinyakov.rtlabstest.db.repository.specification;


import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
public class RTSpecification<T> implements Specification<T> {

    private SearchCriteria criteria;

    @Override
    public Predicate toPredicate(Root<T> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder cb) {


        if (criteria.getKey().equals("createDateFrom")) {
            return cb.greaterThanOrEqualTo(root.get("createDate"), (LocalDate) criteria.getValue());
        }
        if (criteria.getKey().equals("createDateTo")) {
            return cb.lessThanOrEqualTo(root.get("createDate"), (LocalDate) criteria.getValue());
        }
        if (criteria.getKey().equals("status")) {
            return cb.equal(root.get("status"), (Boolean) criteria.getValue());
        }
        if (criteria.getKey().equals("serviceType")) {
            return cb.equal(root.get("service").get("type"), (String) criteria.getValue());
        }
        if (criteria.getKey().equals("applicantFirstName")) {
            return cb.equal(root.get("applicant").get("firstName"), (String) criteria.getValue());
        }
        if (criteria.getKey().equals("applicantLastName")) {
            return cb.equal(root.get("applicant").get("lastName"), (String) criteria.getValue());
        }
        if (criteria.getKey().equals("applicantMiddleName")) {
            return cb.equal(root.get("applicant").get("middleName"), (String) criteria.getValue());
        }
        if (criteria.getKey().equals("applicantPasportNumber")) {
            return cb.equal(root.get("applicant").get("pasportNumber"), (String) criteria.getValue());
        }

        return null;
    }
}