package ru.zinyakov.rtlabstest.db.repository.specification;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class SearchCriteria {
    private String key;
    private Object value;
}
