package ru.zinyakov.rtlabstest.db.repository.specification;


import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.data.jpa.domain.Specification;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
public class SpecificationBuilder<T> {
    private List<SearchCriteria> criteriaList = new ArrayList<>();

    public void add(String key, Object value) {

        criteriaList.add(new SearchCriteria()
                .setKey(key)
                .setValue(value));
    }

    public Specification<T> buildSpecification() {
        if (criteriaList.size() == 0) {
            return null;
        }

        List<Specification<T>> specifications = new ArrayList<>();
        for (SearchCriteria criteria : criteriaList) {
            specifications.add(new RTSpecification<T>(criteria));
        }

        Specification<T> result = specifications.get(0);

        for (Specification<T> specification : specifications) {
            result = Specification.where(result).and(specification);
        }
        return result;
    }
}
