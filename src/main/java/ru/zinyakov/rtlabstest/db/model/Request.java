package ru.zinyakov.rtlabstest.db.model;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table(name = "request_tab")
@Data
@Accessors(chain = true)
public class Request {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long id;

    @Column(name = "create_date")
    @Convert(converter = Jsr310JpaConverters.LocalDateConverter.class)
    private LocalDate createDate;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.DETACH)
    @JoinColumn(name = "applicant_id")
    private Applicant applicant;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.DETACH)
    @JoinColumn(name = "service_id")
    private Service service;

    @OneToMany(fetch = FetchType.LAZY,
            mappedBy = "request")
    private List<AttachedFile> attachedFiles = new ArrayList<>();

    @Column (name = "status")
    Boolean status;


}
