package ru.zinyakov.rtlabstest.db.model;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Entity
    @Table(name = "service_tab")
    @Data
    @Accessors(chain = true)
    public class Service {

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column
        private Long id;

        @Column(name = "type")
        private String type;

    @OneToMany(fetch = FetchType.LAZY,
            cascade = {CascadeType.ALL},
            mappedBy = "service")
    private List<Request> requests = new ArrayList<>();



}
