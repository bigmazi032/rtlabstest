package ru.zinyakov.rtlabstest.db.model;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;


@Entity
@Table(name = "file_tab")
@Data
@Accessors(chain = true)
public class AttachedFile {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "path")
    private String path;

    @ManyToOne (fetch = FetchType.LAZY,
            cascade = {CascadeType.ALL})
    @JoinColumn(name = "request_id")
    private Request request;


}