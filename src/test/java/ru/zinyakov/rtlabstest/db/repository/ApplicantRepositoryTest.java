package ru.zinyakov.rtlabstest.db.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.zinyakov.rtlabstest.db.model.Applicant;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest

public class ApplicantRepositoryTest {
    @Autowired ApplicantRepository applicantRepository;

    @Test
    public void addNewApplicantTest (){
        Applicant applicant = new Applicant()
                .setFirstName("Петр")
                .setLastName("Петорович")
                .setMiddleName("Петров")
                .setPasportNumber("0123456789");
        applicant = applicantRepository.save(applicant);

        assertNotNull(applicant.getId());
    }

    @Test
    public void findAllByCriteria(){

        Applicant applicant1 = new Applicant()
                .setFirstName("Петр")
                .setLastName("Петорович")
                .setMiddleName("Петров")
                .setPasportNumber("0123456789");
        applicant1 = applicantRepository.save(applicant1);
        Applicant applicant2 = new Applicant()
                .setFirstName("Петр")
                .setLastName("Петорович")
                .setMiddleName("Иванов")
                .setPasportNumber("0123456788");
        applicant2 = applicantRepository.save(applicant2);

//        RTSpecification<Applicant> spec = new RTSpecification<>(
//                new SearchCriteria("firstName", SearchOperation.EQUALITY, "Петр"));
//
//        RTSpecification<Applicant> spec2 = new RTSpecification<>(
//                new SearchCriteria("middleName", SearchOperation.EQUALITY, "Иванов"));
//
//        List<Applicant> applicantList = applicantRepository.findAll(Specification.where(spec));
//
//
//        List<Request> requestList = new ArrayList<>();
//        for (Applicant app : applicantList) {
//            List<Request> tempList = app.getRequests();
//            if (tempList!=null) {
//                requestList.addAll(tempList);
//            }
//        }
//        assertThat(applicant2, isIn(applicantList));

    }

}