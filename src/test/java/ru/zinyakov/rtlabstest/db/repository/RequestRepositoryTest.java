package ru.zinyakov.rtlabstest.db.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.test.context.junit4.SpringRunner;
import ru.zinyakov.rtlabstest.db.model.Applicant;
import ru.zinyakov.rtlabstest.db.model.Request;
import ru.zinyakov.rtlabstest.db.model.Service;
import ru.zinyakov.rtlabstest.db.repository.specification.RTSpecification;
import ru.zinyakov.rtlabstest.db.repository.specification.SearchCriteria;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsIn.isIn;


@RunWith(SpringRunner.class)
@SpringBootTest
public class RequestRepositoryTest {
    @Autowired
    RequestRepository requestRepository;
    @Autowired
    ApplicantRepository applicantRepository;
    @Autowired
    ServiceRepository serviceRepository;





    @Test
    @Transactional
    public void findByCriteriaCreateDate() {

        Applicant applicant = new Applicant()
                                .setFirstName("Петр")
                                .setLastName("Петорович")
                                .setMiddleName("Петров")
                                .setPasportNumber("0123456789");
        applicant = applicantRepository.save(applicant);
        Service service = new Service()
                            .setType("Услуга10");

        service = serviceRepository.save(service);
        Request request = new Request()
                                .setCreateDate(LocalDate.now())
                                .setApplicant(applicant)
                                .setService(service)
                                .setStatus(true);
        requestRepository.save(request);
        RTSpecification spec = new RTSpecification(
                new SearchCriteria("createDate", LocalDate.now().minusDays(100)));

        List<Request> results = requestRepository.findAll(Specification.where(spec));

        assertThat(request, isIn(results));

    }

}